#! /bin/bash

# script for CMPT 433 group project

sudo systemctl stop prj.service

sudo systemctl disable prj.service

sudo yes | cp prj.service /lib/systemd/system

sudo rm -f /home/debian/prj

sudo yes | cp prj /home/debian/

sudo systemctl start prj.service

sudo systemctl enable prj.service

systemctl status prj.service

sudo reboot