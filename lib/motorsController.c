#include "motorsDriver.h"
#include "utils.h"
#include "wheelEncoder.h"
#include "collisionDetector.h"

#include <stdio.h>

// todo: calibrate
#define ONE_FORWARD 40
#define HALF_TURN 15
// one rotation is 40
// usually overshoots by 5

int prev_target = 0;

void moveStop(void) {
    setLeftMotor(0);
    setRightMotor(0);
}

// motor doesn't stop instantly
// before moving again, check how much motor has moved excessively
static int getOvershoot() {
    return wheelEncoder_getRotations(0)-prev_target;
}

static void reverse(int target) {
    printf("reverse %d\n", target);
    setLeftMotor(2);
    setRightMotor(2);
    // encoder only goes up
    int end = wheelEncoder_getRotations(0) + target;
    prev_target = end;
    while(wheelEncoder_getRotations(0) <= end){
        sleep_ms(10);
    }
    moveStop();
    collision_reset();
}

int moveForward(void) {
    printf("Moving Forward\n");
    setLeftMotor(1);
    setRightMotor(1);
    //todo: maintain straight heading
    
    //printf("Finish: %d\n", wheelEncoder_getRotations(0));
    //printf("Overshoot Error: +%d\n", wheelEncoder_getRotations(0)-prev_target);
    int start = wheelEncoder_getRotations(0);
    //printf("\nStart: %d\n", start);
    int end = start + ONE_FORWARD-getOvershoot();
    //printf("Adjusted Target: +%d\n", end-start);
    prev_target = end;
    while(wheelEncoder_getRotations(0) < end){
        if (hasCollided() == 0) {
            sleep_ms(10);
        } else {
            reverse(wheelEncoder_getRotations(0)-start);
            return 1;
        }
    }

    return 0;
}
    //0 = right motor
    //1 = left motor
void turnLeft(void) {
    printf("Turning Left\n");
    int end = wheelEncoder_getRotations(0) + HALF_TURN;
    setLeftMotor(2);
    while(end - wheelEncoder_getRotations(0) >= 0){
        sleep_ms(10);
    }
    moveStop();

    end = wheelEncoder_getRotations(1) + HALF_TURN;
    setRightMotor(1);
    while(end - wheelEncoder_getRotations(1) >= 0){
        sleep_ms(10);
    }
    moveStop(); 
}

void turnRight(void) {
    printf("Turning Right\n");
    int end = wheelEncoder_getRotations(1) + HALF_TURN;
    setRightMotor(2);
    while(end - wheelEncoder_getRotations(1) >= 0){
        sleep_ms(10);
    }
    moveStop();
    
    end = wheelEncoder_getRotations(0) + HALF_TURN;
    setLeftMotor(1);
    while(end - wheelEncoder_getRotations(0) >= 0){
        sleep_ms(10);
    }
    moveStop();
}



void MotorsController_init(void) {
    printf("MotorsController_init\n");
    initializeMotorsDriver();
    moveStop();
    sleep_ms(1000);
}

void stopMotorsController(void) {
}
