#ifndef _MOTORSDRIV_H_
#define _MOTORSDRIV_H_

// exports pins P8_7 to P8_12
void initializeMotorsDriver(void);

// 0 = off, 1 = forward, 2 = backward
void setLeftMotor(int mode);
void setRightMotor(int mode);

#endif