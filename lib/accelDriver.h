#include <stdint.h>

// call before use
void initializeAccelerometer(void);

// raw values from registers 0x00 to 0x06, returns size 7 char array
unsigned char * getRawReadings(void);

// x y z axis readings, returns size 3 int array
int16_t * getReadings(void);

// call to free internal buffer before exiting
void uninitializeAccelerometer(void);