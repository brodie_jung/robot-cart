#include "joystickDriver.h"
#include "utils.h"

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>		// Errors
#include <string.h>
#include <sys/epoll.h>  // for epoll()
#include <fcntl.h>      // for open()
#include <unistd.h>     // for close()

#define L_JOYSTICK_DIRECTION "/sys/class/gpio/gpio65/direction"
#define L_JOYSTICK_EDGE      "/sys/class/gpio/gpio65/edge"
#define L_JOYSTICK_IN        "/sys/class/gpio/gpio65/value"
#define R_JOYSTICK_DIRECTION "/sys/class/gpio/gpio47/direction"
#define R_JOYSTICK_EDGE      "/sys/class/gpio/gpio47/edge"
#define R_JOYSTICK_IN        "/sys/class/gpio/gpio47/value"
#define U_JOYSTICK_DIRECTION "/sys/class/gpio/gpio26/direction"
#define U_JOYSTICK_EDGE      "/sys/class/gpio/gpio26/edge"
#define U_JOYSTICK_IN        "/sys/class/gpio/gpio26/value"
#define D_JOYSTICK_DIRECTION "/sys/class/gpio/gpio46/direction"
#define D_JOYSTICK_EDGE      "/sys/class/gpio/gpio46/edge"
#define D_JOYSTICK_IN        "/sys/class/gpio/gpio46/value"


void initializeJoystick(void) {
    exportPin(65);
    exportPin(47);
    exportPin(26);
    exportPin(46);

	writeToFile(L_JOYSTICK_DIRECTION, "in");
	writeToFile(R_JOYSTICK_DIRECTION, "in");
	writeToFile(U_JOYSTICK_DIRECTION, "in");
	writeToFile(D_JOYSTICK_DIRECTION, "in");

	writeToFile(L_JOYSTICK_EDGE, "both");
	writeToFile(R_JOYSTICK_EDGE, "both");
	writeToFile(U_JOYSTICK_EDGE, "both");
	writeToFile(D_JOYSTICK_EDGE, "both");
    printf("joystickDriver initialized\n");
}

// epoll and writes to buffers for each joystick direction 
static int waitForJoystickEdge(void) {
    const char* left = L_JOYSTICK_IN;
    const char* right = R_JOYSTICK_IN;
    const char* up = U_JOYSTICK_IN;
    const char* down = D_JOYSTICK_IN;

	// create an epoll instance
	int epollfd = epoll_create(4);
	if (epollfd == -1) {
		fprintf(stderr, "ERROR: epoll_create() returned (%d) = %s\n", epollfd, strerror(errno));
		return -1;
	}

	// open GPIO value file:
	int fdLeft = open(left, O_RDONLY | O_NONBLOCK);
	if (fdLeft == -1) {
		fprintf(stderr, "ERROR: unable to open() GPIO value file (%d) = %s\n", fdLeft, strerror(errno));
		return -1;
	}
	// open GPIO value file:
	int fdRight = open(right, O_RDONLY | O_NONBLOCK);
	if (fdRight == -1) {
		fprintf(stderr, "ERROR: unable to open() GPIO value file (%d) = %s\n", fdRight, strerror(errno));
		return -1;
	}
	// open GPIO value file:
	int fdUp = open(up, O_RDONLY | O_NONBLOCK);
	if (fdUp == -1) {
		fprintf(stderr, "ERROR: unable to open() GPIO value file (%d) = %s\n", fdUp, strerror(errno));
		return -1;
	}
	// open GPIO value file:
	int fdDown = open(down, O_RDONLY | O_NONBLOCK);
	if (fdDown == -1) {
		fprintf(stderr, "ERROR: unable to open() GPIO value file (%d) = %s\n", fdDown, strerror(errno));
		return -1;
	}

	// configure epoll to wait for events: read operation | edge triggered | urgent data
	struct epoll_event epollStruct, eventsArray[4];
	epollStruct.events = EPOLLIN | EPOLLET | EPOLLPRI;
	epollStruct.data.fd = fdLeft;

	// register file descriptor on the epoll instance, see: man epoll_ctl
	if (epoll_ctl(epollfd, EPOLL_CTL_ADD, fdLeft, &epollStruct) == -1) {
		fprintf(stderr, "ERROR: epoll_ctl() failed on add control interface (%d) = %s\n", fdLeft, strerror(errno));
		return -1;
	}
	if (epoll_ctl(epollfd, EPOLL_CTL_ADD, fdRight, &epollStruct) == -1) {
		fprintf(stderr, "ERROR: epoll_ctl() failed on add control interface (%d) = %s\n", fdRight, strerror(errno));
		return -1;
	}
	if (epoll_ctl(epollfd, EPOLL_CTL_ADD, fdUp, &epollStruct) == -1) {
		fprintf(stderr, "ERROR: epoll_ctl() failed on add control interface (%d) = %s\n", fdUp, strerror(errno));
		return -1;
	}
	if (epoll_ctl(epollfd, EPOLL_CTL_ADD, fdDown, &epollStruct) == -1) {
		fprintf(stderr, "ERROR: epoll_ctl() failed on add control interface (%d) = %s\n", fdDown, strerror(errno));
		return -1;
	}

	// ignore first trigger
	for (int i = 0; i < 2; i++) {
		int waitRet = epoll_wait(
				epollfd, 
				eventsArray, 
				4,                // maximum # events
				-1);              // timeout in ms, -1 = wait indefinite; 0 = returne immediately

		if (waitRet == -1){
			fprintf(stderr, "ERROR: epoll_wait() failed (%d) = %s\n", waitRet, strerror(errno));
			close(fdLeft);
			close(fdRight);
			close(fdUp);
			close(fdDown);
			close(epollfd);
			return -1;
		}
	}

	// cleanup epoll instance:
	close(fdLeft);
	close(fdRight);
	close(fdUp);
	close(fdDown);
	close(epollfd);
	return 0;
}

// N E S W = 0 1 2 3
int getJoystickInput(void) {
    // Wait for an edge trigger:
    int ret = waitForJoystickEdge();
    if (ret == -1) {
        exit(EXIT_FAILURE);
    }

    // Current state:
    char L_buff[1024], R_buff[1024], U_buff[1024], D_buff[1024];
    int bytesRead = readLineFromFile(L_JOYSTICK_IN, L_buff, 1024);
    if (bytesRead > 0) {
        if (L_buff[0] == '0') {
            return 3;
        }
    } else {
        fprintf(stderr, "ERROR: Read 0 bytes from GPIO input: %s\n", strerror(errno));
    }
    bytesRead = readLineFromFile(R_JOYSTICK_IN, R_buff, 1024);
    if (bytesRead > 0) {
        if (R_buff[0] == '0') {
            return 1;
        }
    } else {
        fprintf(stderr, "ERROR: Read 0 bytes from GPIO input: %s\n", strerror(errno));
    }
    bytesRead = readLineFromFile(U_JOYSTICK_IN, U_buff, 1024);
    if (bytesRead > 0) {
        if (U_buff[0] == '0') {
            return 0;
        }
    } else {
        fprintf(stderr, "ERROR: Read 0 bytes from GPIO input: %s\n", strerror(errno));
    }
    bytesRead = readLineFromFile(D_JOYSTICK_IN, D_buff, 1024);
    if (bytesRead > 0) {
        if (D_buff[0] == '0') {
            return 2;
        }
    } else {
        fprintf(stderr, "ERROR: Read 0 bytes from GPIO input: %s\n", strerror(errno));
    }
    
    return -1;
}