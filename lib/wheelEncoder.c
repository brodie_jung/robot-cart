#include "wheelEncoder.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#include <pthread.h>

#define LED_DIRECTION "/sys/class/leds/leds:P9.23"
//"/sys/class/gpio/gpio49/direction"
#define LED_VALUE "/sys/class/leds/leds:P9.23/brightness"
//"/sys/class/gpio/gpio49/value"

#define A2D_FILE_VOLTAGE1 "/sys/bus/iio/devices/iio:device0/in_voltage1_raw"
#define A2D_FILE_VOLTAGE2 "/sys/bus/iio/devices/iio:device0/in_voltage3_raw"
#define A2D_VOLTAGE_REF 1.8
#define A2D_MAX_READING 4095

//static void setPin(const char* filename,const char* value);
void* wheelEncoder_thread(void* arg);
static pthread_t wheelEncoderThreadId;
static bool stopping = false;

static double average1 = 0;
static double average2 = 0;

static bool over1 = false;
static bool over2 = false;

static int dip1 = 0;
static int dip2 = 0;

//Sets the value of the given filename
//Code modified from GPIO guide
/* static void setPin(const char* filename,const char* value){
    FILE *pFile = fopen(filename,"w");
    if (pFile == NULL){
        printf("ERROR: Unable to open export file\n");
        exit(-1);
    }

    fprintf(pFile, "%s", value);
    fclose(pFile);
} */

//Code taken from A2D guide
int wheelEncoder_getVolatageReading1(){
    FILE *pFile = fopen(A2D_FILE_VOLTAGE1,"r");
    if (pFile == NULL){
        printf("ERROR: Unable to open voltage input file\n");
        exit(1);
    }

    int a2dReading = 0;
    int itemsRead = fscanf(pFile, "%d", &a2dReading);
    if(itemsRead <= 0){
        printf("ERROR: Unable to read values from voltage input file\n");
        exit(-1);
    }
    fclose(pFile);
    return a2dReading;
}

//Code taken from A2D guide
int wheelEncoder_getVolatageReading2(){
    FILE *pFile = fopen(A2D_FILE_VOLTAGE2,"r");
    if (pFile == NULL){
        printf("ERROR: Unable to open voltage input file\n");
        exit(1);
    }

    int a2dReading = 0;
    int itemsRead = fscanf(pFile, "%d", &a2dReading);
    if(itemsRead <= 0){
        printf("ERROR: Unable to read values from voltage input file\n");
        exit(-1);
    }
    fclose(pFile);
    return a2dReading;
}

int wheelEncoder_getRotations(int side){
    if(side == 0){
        return dip1;
    }
    else if(side == 1){
        return dip2;
    }
    return 0;
}

void wheelEncoder_init(void){
    /* setPin("/sys/class/gpio/export", "49");
    setPin(LED_DIRECTION, "out");
    setPin(LED_VALUE, "0"); */

    pthread_create(&wheelEncoderThreadId, NULL, wheelEncoder_thread,NULL);
}

void wheelEncoder_shutdown(void){
    printf("Calling shutdown of wheelEncoder\n");
    stopping = true;
    pthread_join(wheelEncoderThreadId, NULL);
}

void* wheelEncoder_thread(void* args){
    long seconds = 0;
    long nanoseconds = 100000; //1 ms
    struct timespec reqDelay = {seconds, nanoseconds};
    average1 = ((double)wheelEncoder_getVolatageReading1() / A2D_MAX_READING) * A2D_VOLTAGE_REF;
    average2 = ((double)wheelEncoder_getVolatageReading2() / A2D_MAX_READING) * A2D_VOLTAGE_REF;
    while(!stopping){
        int reading = wheelEncoder_getVolatageReading1();
        double voltage1 = ((double)reading / A2D_MAX_READING) * A2D_VOLTAGE_REF;
        average1 = (average1*0.99) + (voltage1*0.01);
        if(!over1){
            if(voltage1 > average1 + 0.005){
                over1 = true;
                dip1++;
            }
        }
        else{
            if(voltage1 < average1 - 0.005){
                over1 = false;
                dip1++;
            }
        }
        //printf("Voltage 1 %fv\n", voltage1);
        //printf("Average 1 %fv\n", average1);
        //printf("Rotations 1 %d\n", dip1);

        reading = wheelEncoder_getVolatageReading2();
        double voltage2 = ((double)reading / A2D_MAX_READING) * A2D_VOLTAGE_REF;
        average2 = (average2*0.99) + (voltage2*0.01);

        if(!over2){
            if(voltage2 > average2 + 0.05){
                over2 = true;
                dip2++;
            }
        }
        else{
            if(voltage2 < average2 - 0.05){
                over2 = false;
                dip2++;
            }
        }

        //printf("Value 2 %fv\n", average2);
        //printf("Value 2 %fv\n", voltage2);
        //printf("Combined %fv\n", voltage2+voltage1);
        //printf("Rotations 2 %d\n", dip2);
        nanosleep(&reqDelay, (struct timespec *) NULL);
    }
    return NULL;
}