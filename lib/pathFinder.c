#include "pathFinder.h"
#include "motorsController.h"
#include "utils.h"

#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdbool.h>
#include <pthread.h>

#define MAX_ROWS 10
#define MAX_COLS 15

#define UNVISITED 0
#define VISITED 1
#define OBSTRUCTION 2
#define BORDER 3

int steps = 0;
int map[MAX_ROWS][MAX_COLS] = {{UNVISITED}};

void *pathFinder_thread(void *arg);
static bool stopping = false;
static pthread_t pathFinderThreadId;
int currRow, currCol;

bool Ndetour = false;
bool Sdetour = false;
int returnAttempts = 0;

void pathFinder_init(void)
{
    printf("pathFinder_init().... \n");
    pathFinder_mapInit();

    pthread_create(&pathFinderThreadId, NULL, pathFinder_thread, NULL);
}

void pathFinder_mapInit(void)
{
    // Initialize borders
    for (int i = 0; i < MAX_ROWS; i++)
    {
        map[i][0] = BORDER;
        map[i][MAX_COLS - 1] = BORDER;
    }
    for (int j = 0; j < MAX_COLS; j++)
    {
        map[0][j] = BORDER;
        map[MAX_ROWS - 1][j] = BORDER;
    }

    // Initialize robot position and facing, TODO: Remove hard-coded values for testing
    currRow = 1;
    currCol = 1;

    // Initialize obstructions, TODO: Remove hard-coded values for testing
    map[5][5] = OBSTRUCTION;
    map[7][6] = OBSTRUCTION;

    map[3][2] = OBSTRUCTION;
    map[4][2] = OBSTRUCTION;

    map[4][8] = OBSTRUCTION;
    map[4][9] = OBSTRUCTION;
    map[4][10] = OBSTRUCTION;    
    map[5][8] = OBSTRUCTION;
    map[5][9] = OBSTRUCTION;
    map[5][10] = OBSTRUCTION;    


}

int pathFinder_checkNextSpace(void)
{
    int nextSpace;
    switch (currDir)
    {
    case N:
        nextSpace = map[currRow + 1][currCol];
        break;
    case S:
        nextSpace = map[currRow - 1][currCol];
        break;
    case W:
        nextSpace = map[currRow][currCol - 1];
        break;
    case E:
        nextSpace = map[currRow][currCol + 1];
        break;
    }

    return nextSpace;
}

int pathFinder_checkPrevSpace(void)
{
    int prevSpace;
    switch (currDir)
    {
    case N:
        prevSpace = map[currRow - 1][currCol];
        break;
    case S:
        prevSpace = map[currRow + 1][currCol];
        break;
    case W:
        prevSpace = map[currRow][currCol + 1];
        break;
    case E:
        prevSpace = map[currRow][currCol - 1];
        break;
    }

    return prevSpace;
}

int pathFinder_checkWestSpace(void) {
    return map[currRow][currCol-1];
}

// robot successfulyl moved forward 1 space
void pathFinder_positionUpdate(void)
{
    switch (currDir)
    {
    case N:
        currRow += 1;
        break;
    case S:
        currRow -= 1;
        break;
    case W:
        currCol -= 1;
        break;
    case E:
        currCol += 1;
        break;
    }

    map[currRow][currCol] = VISITED;
}

// robot collided
void pathFinder_collisionUpdate(void)
{
    switch (currDir)
    {
    case N:
        map[currRow+1][currCol] = OBSTRUCTION;
        break;
    case S:
        map[currRow-1][currCol] = OBSTRUCTION;

        break;
    case W:
        map[currRow][currCol-1] = OBSTRUCTION;

        break;
    case E:
        map[currRow][currCol+1] = OBSTRUCTION;
        break;
    }
}

static void printMap(void)
{
    printf("\nSteps: %d\n", steps);
    for (int i = MAX_ROWS-1; i >= 0; i--)
    {
        for (int j = 0; j < MAX_COLS; j++)
        {
            if (i == currRow && j == currCol)
            {
                switch (currDir)
                {
                case N:
                    printf("▲ ");
                    break;
                case S:
                    printf("▼ ");
                    break;
                case W:
                    printf("◀ ");
                    break;
                case E:
                    printf("▶ ");
                    break;
                default:
                    break;
                }
            }
            else
            {
                switch (map[i][j])
                {
                case UNVISITED:
                    printf("- ");
                    break;
                case VISITED:
                    printf("# ");
                    break;
                case OBSTRUCTION:
                    printf("▣ ");
                    break;
                case BORDER:
                    printf("▩ ");
                    break;
                default:
                    break;
                }
            }
        }
        printf("\n");
    }
    printf("\n");
}

static void pathfinder_turnLeft() {
    //motor call
    turnLeft();

    switch (currDir)
    {
    case N:
        currDir = W;
        break;
    case S:
        currDir = E;
        break;
    case W:
        currDir = S;
        break;
    case E:
        currDir = N;
        break;
    }
}

static void pathfinder_turnRight() {
    //motor call
    turnRight();

    switch (currDir)
    {
    case N:
        currDir = E;
        break;
    case S:
        currDir = W;
        break;
    case W:
        currDir = N;
        break;
    case E:
        currDir = S;
        break;
    }
}

void *pathFinder_thread(void *args)
{
    while (!stopping)
    {
        steps++;
        sleep_ms(500); //todo remove
        printMap();
        int nextSpaceStatus = pathFinder_checkNextSpace();
        // map reached end
        if( pathFinder_checkPrevSpace() == VISITED && (nextSpaceStatus == BORDER || nextSpaceStatus == OBSTRUCTION) && currCol == MAX_COLS - 2)
        {
            printf("Robot reached the end!\n");
            stopping = true;
            break;
        }

        switch (nextSpaceStatus)
        {
        case VISITED:
            if (Ndetour == true) {
                pathfinder_turnRight();
                Ndetour = false;
                break;
            } else if (Sdetour == true)
            {
                pathfinder_turnLeft();
                Sdetour = false;
                break;
            }
        case UNVISITED:
            if (moveForward() == 0) {
                // success
                pathFinder_positionUpdate();
                if (currDir == N && Ndetour == true) 
                {
                    // if we avoided a obstacle earlier, move back when free
                    if (pathFinder_checkWestSpace() == UNVISITED) {
                        pathfinder_turnLeft();
                    }                     
                } else if (currDir == S && Sdetour == true) 
                {
                    // if we avoided a obstacle earlier, move back when free
                    if (pathFinder_checkWestSpace() == UNVISITED) {
                        pathfinder_turnRight();
                    }                     
                } else if (currDir == E) 
                {
                    // check where we came from
                    if (Ndetour == true)
                        pathfinder_turnLeft();
                    else if (Sdetour == true) {
                        pathfinder_turnRight();
                    }
                }            
            } else {
                // collision
                pathFinder_collisionUpdate();
            }
            break;

        case BORDER: //intentional
        case OBSTRUCTION:
            
                switch (currDir)
                {
                    case N:
                        if (Ndetour == true) {
                            pathfinder_turnRight();
                            pathfinder_turnRight();
                            Ndetour = false;
                        } else {
                            pathfinder_turnRight();
                            Ndetour = true;
                        }
                        break;
                    case S:
                        if (Sdetour == true) {
                            pathfinder_turnLeft();
                            pathfinder_turnLeft();
                            Sdetour = false;
                        } else {
                            pathfinder_turnLeft();
                            Sdetour = true;
                        }
                        break;
                    case W:
                        break;
                    case E:
                        break;
                }
            
            break;
        }
    }
    return NULL;
}
