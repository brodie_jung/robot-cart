#ifndef _PATHFINDER_H_
#define _PATHFINDER_H_

enum currDirEnum
{
    N,
    S,
    W,
    E
} currDir;

enum lastMoveEnum
{
    Forwarded,
    Reversed,
    TurnedLeft,
    TurnRight
} lastMove;


void pathFinder_init(void);
void pathFinder_shutdown(void);
void pathFinder_mapInit(void);
int pathFinder_checkNextSpace(void);
void pathFinder_positionUpdate(void);


#endif