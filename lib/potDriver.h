// potDriver.h
#ifndef _POTDRIVER_H_
#define _POTDRIVER_H_
// read analog input voltage 0 on the BeagleBone
int getPotVoltage0Reading(void);
#endif