#include "accelDriver.h"
#include "utils.h"

#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <stdatomic.h>
#include <stdbool.h>

#define MAX_READING 32768
#define COLLISION_THRESHOLD 12000
pthread_t tid;
atomic_bool detecting;
atomic_bool COLLIDED;

void collision_reset(void) {
    COLLIDED = false;
}

int hasCollided(void) {
    if (COLLIDED == true) {
        return 1;
    } else {
        return 0;
    }
}

static void check_acceleration(int16_t * xyz) {
    // depends on how board is mounted
    //int x = abs(xyz[0]);
    int y = abs(xyz[1]);
    //printf("x: %d\n", x);
    //printf("y: %d\n", y);

    if (y > COLLISION_THRESHOLD) {
        COLLIDED = true;
        printf("COLLISION, force: %d\n", y);
    }
}


static void *detect_collision(void * arg) {
    int16_t * xyz;
    while (detecting) {
        if (COLLIDED == false) {
            xyz = getReadings();
            check_acceleration(xyz);
        }

        sleep_ms(5);
    }
    free(xyz);
    pthread_exit(NULL);
}

void CollisionDetector_init(void) {
    initializeAccelerometer();
    detecting = true;

    if (pthread_create(&tid, NULL, detect_collision, NULL) != 0) {
        perror("detect_collision pthread_create() error");
        exit(1);
    }
    printf("CollisionDetector started\n");
}

void CollisionDetector_stop(void) {
    detecting = false;
    pthread_join(tid, NULL);
    uninitializeAccelerometer();
}