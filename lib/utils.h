#ifndef _UTILS_
#define _UTILS_

// non-blocking sleep in milliseconds 
void sleep_ms(unsigned int delayMs);

// export a GPIO pin for use
void exportPin(int pin);

// write char array to file
void writeToFile(const char *fileName, const char *value);

// read a specified number of bytes from file, caller provide buffer
int readLineFromFile(char *fileName, char *buff, unsigned int maxLength);

// read value from file, returns pointer to newly allocated buffer
// caller must free
char *readValueFromFile(char *fileName, int maxLength);

// opens I2c bus for R/W as slave
int initI2cBus(char *bus, int address);

// read/write to register at specified I2c address
void writeI2cReg(int i2cFileDesc, unsigned char regAddr, unsigned char value, int nbytes);
void readI2cReg(int i2cFileDesc, unsigned char regAddr, unsigned char **values);

#endif
