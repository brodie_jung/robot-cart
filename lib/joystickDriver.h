#ifndef _JOYDRIVER_H_
#define _JOYDRIVER_H_

// initialize pins
void initializeJoystick(void);

// gets current joystick direction as simple integer 0-3
// N E S W = 0 1 2 3
int getJoystickInput(void);

#endif