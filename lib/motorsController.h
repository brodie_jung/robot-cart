// move one car length forward
/// returns 0 if successful
/// return 1 if collision
//// will reverse the car to original spot
//// before moveForward was called
int moveForward(void);

void moveStop(void);


// turn -90 degrees
void turnLeft(void);

// turn 90 degrees
void turnRight(void);


void MotorsController_init(void);

void stopMotorsController(void);