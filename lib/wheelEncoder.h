#ifndef _WHEELENCODER_H_
#define _WHEELENCODER_H_

void wheelEncoder_init(void);
int wheelEncoder_getVolatageReading1(void);
int wheelEncoder_getVolatageReading2(void);
int wheelEncoder_getRotations(int side);
void wheelEncoder_shutdown(void);

#endif