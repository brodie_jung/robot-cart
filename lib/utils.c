#include "utils.h"

#include <time.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>
#include <sys/fcntl.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

// sleep for x ms
void sleep_ms(unsigned int delayMs)
{
    const unsigned int NS_PER_MS = 1000 * 1000;
    const unsigned int NS_PER_SECOND = 1000000000;

    unsigned long long delayNs = delayMs * NS_PER_MS;
    int seconds = delayNs / NS_PER_SECOND;
    int nanoseconds = delayNs % NS_PER_SECOND;

    struct timespec reqDelay = {seconds, nanoseconds};
    nanosleep(&reqDelay, (struct timespec *)NULL);
}

void exportPin(int pin)
{
    FILE *pFile = fopen("/sys/class/gpio/export", "w");
    if (pFile == NULL)
    {
        printf("ERROR: Unable to open export file.\n");
        exit(1);
    }
    // Write to data to the file using fprintf():
    fprintf(pFile, "%d", pin);
    // Close the file using fclose():
    fclose(pFile);
    // Call nanosleep() to sleep for ~300ms before use.
    sleep_ms(300);
}

void writeToFile(const char *fileName, const char *value)
{
    FILE *pFile = fopen(fileName, "w");
    fprintf(pFile, "%s", value);
    fclose(pFile);
}

int readLineFromFile(char *fileName, char *buff, unsigned int maxLength)
{
    FILE *file = fopen(fileName, "r");
    int bytes_read = getline(&buff, &maxLength, file);
    fclose(file);
    return bytes_read;
}

char *readValueFromFile(char *fileName, int maxLength)
{
    FILE *file = fopen(fileName, "r");
    char *value = malloc(maxLength);
    fscanf(file, "%s", value);
    fclose(file);
    return value;
}

int initI2cBus(char *bus, int address)
{
    int i2cFileDesc = open(bus, O_RDWR);
    if (i2cFileDesc < 0)
    {
        printf("I2C DRV: Unable to open bus for read/write (%s)\n", bus);
        perror("Error is:");
        exit(-1);
    }

    int result = ioctl(i2cFileDesc, I2C_SLAVE, address);
    if (result < 0)
    {
        perror("Unable to set I2C device to slave address.");
        exit(-1);
    }
    return i2cFileDesc;
}

void writeI2cReg(int i2cFileDesc, unsigned char regAddr, unsigned char value, int nbytes)
{
    unsigned char buff[2];
    buff[0] = regAddr;
    buff[1] = value;
    int res = write(i2cFileDesc, buff, 2);
    if (res != 2)
    {
        perror("Unable to write i2c register");
        exit(-1);
    }
}

void readI2cReg(int i2cFileDesc, unsigned char regAddr, unsigned char **values)
{
    // To read a register, must first write the address
    int res = write(i2cFileDesc, &regAddr, sizeof(regAddr));
    if (res != sizeof(regAddr))
    {
        perror("I2C: Unable to write to i2c register.");
        exit(1);
    }
    free(*values);
    *values = malloc(7 * sizeof(char)); // todo: free() in caller on exit
    // Now read the value and return it
    res = read(i2cFileDesc, *values, 7 * sizeof(char));
    if (res != 7 * sizeof(char))
    {
        perror("I2C: Unable to read from i2c register");
        exit(1);
    }
}
