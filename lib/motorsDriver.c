#include "motorsDriver.h"
#include "utils.h"

#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>

// P8_7
#define D66 "/sys/class/gpio/gpio66/direction"
#define V66 "/sys/class/gpio/gpio66/value"
// P8_8
#define D67 "/sys/class/gpio/gpio67/direction"
#define V67 "/sys/class/gpio/gpio67/value"
// P8_9
#define D69 "/sys/class/gpio/gpio69/direction"
#define V69 "/sys/class/gpio/gpio69/value"
// P8_10
#define D68 "/sys/class/gpio/gpio68/direction"
#define V68 "/sys/class/gpio/gpio68/value"
// P8_11
#define D45 "/sys/class/gpio/gpio45/direction"
#define V45 "/sys/class/gpio/gpio45/value"
// P8_12
#define D44 "/sys/class/gpio/gpio44/direction"
#define V44 "/sys/class/gpio/gpio44/value"

void initializeMotorsDriver(void) {
    printf("initializeMotorsDriver\n");
    exportPin(66);
    exportPin(67);
    exportPin(69);
    exportPin(68);
    exportPin(45);
    exportPin(44);

    writeToFile(D66, "out");
    writeToFile(D67, "out");
    writeToFile(D69, "out");
    writeToFile(D68, "out");
    writeToFile(D45, "out");
    writeToFile(D44, "out");

    //todo: pwm speed control
    writeToFile(V45, "1");
    writeToFile(V44, "1");
}

void setLeftMotor(int mode) {
    // 0 = off, 1 = forward, 2 = backward
    switch (mode)
    {
    case 0:
        writeToFile(V69, "0");
        writeToFile(V68, "0");
        break;
    case 1:
        writeToFile(V69, "1");
        writeToFile(V68, "0");
        break;
    case 2:
        writeToFile(V69, "0");
        writeToFile(V68, "1");
        break;        
    default:
        writeToFile(V69, "0");
        writeToFile(V68, "0");
        break;
    }
}

void setRightMotor(int mode) {
    // 0 = off, 1 = forward, 2 = backward
    switch (mode)
    {
    case 0:
        writeToFile(V66, "0");
        writeToFile(V67, "0");
        break;
    case 1:
        writeToFile(V66, "0");
        writeToFile(V67, "1");
        break;
    case 2:
        writeToFile(V66, "1");
        writeToFile(V67, "0");
        break;        
    default:
        writeToFile(V66, "0");
        writeToFile(V67, "0");    
        break;
    }
}