// call this to detect collisions again
void collision_reset(void);

// repeatedly make calls to this
/// returns 1 if there has been a collision
/// returns 0 otherwise
int hasCollided(void);

void CollisionDetector_init(void);