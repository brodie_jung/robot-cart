#include "accelDriver.h"
#include "utils.h"

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>		// Errors
#include <string.h>
#include <sys/epoll.h>  // for epoll()
#include <fcntl.h>      // for open()
#include <unistd.h>     // for close()

//I2C bus I2C_1 at address 0x1C.
#define I2C_DEVICE_ADDRESS_0 0x1C
#define I2C_DEVICE_ADDRESS_1 0x1D
#define I2CDRV_LINUX_BUS1 "/dev/i2c-1"

#define WHO_AM_I 0x0D
#define STATUS 0x00
#define OUT_X_MSB 0x01
#define OUT_X_LSB 0x02
#define OUT_Y_MSB 0x03
#define OUT_Y_LSB 0x04
#define OUT_Z_MSB 0x05
#define OUT_Z_LSB 0x06
#define SYSMOD 0x0B
#define CTRL_REG1 0x2A
#define CTRL_REG2 0x2B
#define CTRL_REG3 0x2C
#define CTRL_REG4 0x2D

int i2cFileDesc;
unsigned char * values;
int16_t * readings;
static void activate(void) {
    writeI2cReg(i2cFileDesc, CTRL_REG2, 0x12, 1);
    writeI2cReg(i2cFileDesc, CTRL_REG1, 0x05, 1);
}

void initializeAccelerometer(void) {
    printf("initialize accelerometer\n");
    // exportPin(4);
    // exportPin(5);
	// system("config-pin P9_18 i2c");
	// system("config-pin P9_17 i2c");
    
    i2cFileDesc = initI2cBus(I2CDRV_LINUX_BUS1, I2C_DEVICE_ADDRESS_0);
    activate();

    readings = malloc(3*sizeof(int16_t));

    readI2cReg(i2cFileDesc, 0x00, &values);
    printf("accelerometer active\n");
    
}

void uninitializeAccelerometer(void) {
    free(readings);
    printf("uninitialize accelerometer\n");
}

unsigned char * getRawReadings(void) {
    readI2cReg(i2cFileDesc, 0x00, &values);
    printf("0x00: = 0x%02x\n", values[0]);
    printf("0x01: = 0x%02x\n", values[1]);
    printf("0x02: = 0x%02x\n", values[2]);
    printf("0x03: = 0x%02x\n", values[3]);
    printf("0x04: = 0x%02x\n", values[4]);
    printf("0x05: = 0x%02x\n", values[5]);
    printf("0x06: = 0x%02x\n\n", values[6]);
    return values;
}

int16_t * getReadings(void) {
    readI2cReg(i2cFileDesc, 0x00, &values);
    readings[0] = (values[1] << 8) | (values[2]);
    readings[1] = (values[3] << 8) | (values[4]);
    readings[2] = (values[5] << 8) | (values[6]);

    //printf("x: %d\n", readings[0]);
    // printf("y: %d\n", readings[1]);
    // printf("z: %d\n\n", readings[2]);

    return readings;
}