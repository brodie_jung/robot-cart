#include "lib/potDriver.h"
#include "lib/utils.h"
#include "lib/wheelEncoder.h"
#include "lib/motorsController.h"
#include "lib/pathFinder.h"
#include "lib/collisionDetector.h"

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>


int main() {

  printf("from main.c \n");
  CollisionDetector_init();
  wheelEncoder_init();
  MotorsController_init();
  pathFinder_init();


  while(true){
    
  }
  wheelEncoder_shutdown();
  pathFinder_shutdown();
  printf("main \n");

  return 0;
}